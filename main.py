from scripts.functions import write_data, read_skill_pack_csv, read_skills_csv

from scripts.mysql_connection import MySQLConnection

conn_obj = MySQLConnection()
conn = conn_obj.create_connection()

def load_skills_esco():
    select_columns = ["reuseLevel", "preferredLabel", "altLabels",
                      "hiddenLabels", "description"]
    file_name = "skills.csv"
    skills_df = read_skills_csv(select_columns, file_name)
    write_data(conn.engine, skills_df, "skills_esco")

def load_skill_pack():
    select_columns = ["skill", "description","skillpack","associated_profiles"]
    file_name = "skill_pack.csv"
    df = read_skill_pack_csv(select_columns, file_name)
    write_data(conn.engine, df, "skills")


if __name__ == '__main__':
    load_skills_esco()
    load_skill_pack()
    conn_obj.close_conection()
