CREATE TABLE `skills_esco` (
  `id` varchar(36) primary key,
  `reuseLevel` varchar(19) DEFAULT NULL,
  `preferredLabel` text DEFAULT NULL,
  `altLabels` text DEFAULT NULL,
  `hiddenLabels` text DEFAULT NULL,
  `description` text DEFAULT null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE `skills` (
  `id` varchar(36) primary key,
  `skill` text default null,
  `description` text default null,
  `skillpack` text default null,
  `associated_profiles` text default null
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- Pregunta 1
SELECT  100 * count(distinct(s.skill)) / (select count(distinct(s2.skill)) from skills s2) as 'porcentaje'
  FROM skills s
 WHERE EXISTS (SELECT 1
                 FROM skills_esco se
                WHERE se.preferredLabel = s.skill);

-- Pregunta 2
SELECT  s.associated_profiles, 100 * count(distinct(s.skill))/( select count(distinct(s2.skill)) from skills s2 where s2.associated_profiles = s.associated_profiles) as 'porcentaje'
  FROM skills s
 WHERE EXISTS (SELECT 1
                 FROM skills_esco se
                WHERE se.preferredLabel = s.skill)
 and associated_profiles is not null
group by s.associated_profiles
order by porcentaje desc;

-- Pregunta 3
SELECT  s.skillpack,  100 * count(distinct(s.skill))/( select count(distinct(s2.skill)) from skills s2 where s2.skillpack = s.skillpack) as 'porcentaje'
  FROM skills s
 WHERE EXISTS (SELECT 1
                 FROM skills_esco se
                WHERE se.preferredLabel = s.skill)
group by s.skillpack
order by porcentaje desc;