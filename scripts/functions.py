import uuid

import pandas as pd


def trim_all_columns(df):
    trim_strings = lambda x: x.strip() if isinstance(x, str) else x
    return df.applymap(trim_strings)


def lower_all_columns(df):
    lower_strings = lambda s: s.lower() if type(s) == str else s
    return df.applymap(lower_strings)


def read_skills_csv(select_columns, file_name):
    df = pd.read_csv("input/" + file_name, encoding='latin-1', usecols=select_columns)
    df.loc[:, 'id'] = df.apply(lambda x: uuid.uuid1(), axis=1)
    df = trim_all_columns(df)
    df = lower_all_columns(df)
    df = df.drop_duplicates(subset="preferredLabel", keep="first")
    df = df[df["preferredLabel"].notnull()]
    return df


def read_skill_pack_csv(select_columns, file_name):
    df = pd.read_csv("input/" + file_name, encoding='latin-1', usecols=select_columns)
    df = trim_all_columns(df)
    df = lower_all_columns(df)
    df = df.drop_duplicates(subset="skill", keep="first")
    df = df[df["skill"].notnull()]
    df = (df.assign(associated_profiles=df.associated_profiles.str.split(','),
                    skillpack=df.skillpack.str.split(','))
          .explode('associated_profiles')
          .explode('skillpack')
          ).reset_index(drop=True)
    df = df.drop_duplicates()
    df.loc[:, 'id'] = df.apply(lambda x: uuid.uuid1(), axis=1)
    return df


def write_data(conn, df, table_name):
    with conn.begin() as conn:
        df.to_sql(table_name, con=conn, schema='test', if_exists='append', index=False)
