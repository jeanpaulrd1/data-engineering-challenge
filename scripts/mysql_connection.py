import configparser
import sqlalchemy
from sqlalchemy import exc

config = configparser.ConfigParser()
config.read('config/db.conf')

class MySQLConnection():
    def __init__(self):
        self.engine = None
        self.__host = config.get('mysql', 'host')
        self.__port = config.get('mysql', 'port')
        self.__user = config.get('mysql', 'user')
        self.__passwd = config.get('mysql', 'passwd')
        self.__db = config.get('mysql', 'db')

    def create_connection(self):
        try:
            self.engine = sqlalchemy.create_engine('mysql+pymysql://' + self.__user + ':' + self.__passwd + '@' +
                                                   self.__host + ':' + self.__port + '/' + self.__db, echo=False)
            return self.engine.connect()
        except exc.OperationalError as err:
            print("Ocurrió un error al intentar conectar a la bd")

    def close_conection(self):
        if self.engine is not None and not type('str'):
            self.engine.close()